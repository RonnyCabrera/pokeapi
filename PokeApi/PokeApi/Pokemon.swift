//
//  Pokemon.swift
//  PokeApi
//
//  Created by Ronny Cabrera on 6/20/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

struct Pokemon: Decodable {
    var id: Int
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
}

struct Sprite: Decodable {
    var defaultSprite:String
    
    enum CodingKeys: String, CodingKey {
        case defaultSprite = "front_default"
    }
}
