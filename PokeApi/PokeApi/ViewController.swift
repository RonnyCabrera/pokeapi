//
//  ViewController.swift
//  PokeApi
//
//  Created by Ronny Cabrera on 6/20/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var pokemonTableView: UITableView!
    let pokemonManager = PokemonManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network: Network = Network()
        network.getAllPokemon(){(pokemonArray) in
            self.pokemonManager.itemsPokemon = pokemonArray
            self.pokemonTableView.reloadData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonManager.itemsPokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemonManager.itemsPokemon[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInfoPokemonSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInfoPokemonSegue" {
            let destination = segue.destination as! InfoPokemonViewController
            let selectedRow = pokemonTableView.indexPathsForSelectedRows![0]
            destination.itemInfoPokemon = (pokemonManager, selectedRow.row)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

