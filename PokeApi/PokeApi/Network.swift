//
//  Network.swift
//  PokeApi
//
//  Created by Ronny Cabrera on 6/20/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class Network {
    
    func getAllPokemon(completion:@escaping ([Pokemon])->()) {
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup()
        
        for i in 1...8 {
            group.enter()
            
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response
                in
                
                guard let data = response.data else {
                    print("error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data)else {
                    print("error decoding Pokemon")
                    return
                }
                
                //print(pokemon)
                pokemonArray.append(pokemon)
                group.leave()
            }
        }
        
        group.notify(queue: .main){
            completion(pokemonArray)
        }
    }
    
    //func getImage(url:String, _ id:Int, completionHandler: @escaping(UIImage)->()){
        //let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        //Alamofire.request(url).responseImage { response in
            //if let image = response.result.value {
                //completionHandler(image)
            //}
        //}
    //}
    
    func getPokemonImage(id:Int, completion:@escaping (UIImage)->()){
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in debugPrint(response)
            
            if let image = response.result.value {
                completion(image)
            }
        }
    }
}
