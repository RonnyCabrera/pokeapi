//
//  InfoPokemonViewController.swift
//  PokeApi
//
//  Created by Ronny Cabrera on 6/20/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class InfoPokemonViewController: UIViewController {

    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    
    var itemInfoPokemon:(itemManagerPokemon: PokemonManager,index: Int)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].name
        let weightTemp:Int = (itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].weight)!
        let heightTemp:Int = (itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].height)!
        weightLabel.text = String(weightTemp)
        heightLabel.text = String(heightTemp)
        
        let url = itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].sprites.defaultSprite
        let bm = Network()
        bm.getPokemonImage(id:(itemInfoPokemon?.itemManagerPokemon.itemsPokemon[(itemInfoPokemon?.index)!].id)!) { (pkImage) in
                
                self.pokemonImageView.image = pkImage
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
